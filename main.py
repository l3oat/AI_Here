from dfs import depthFirstSearch
from bfs import breadthFirstSearch
from search_algorithm import getPath
from simulatekey import sendKeySimulate
from map_generator import mapGen
from map import mapList

mapID = 'H4'
# mapID = input('Enter map id : ')
graphMap, startMap = mapGen(mapList[mapID])
# path = breadthFirstSearch(graphMap,startMap)
path = depthFirstSearch(graphMap,startMap)
# print(getPath(path))
sendKeySimulate(getPath(path))

