from search_algorithm import Queue,PathNode,state_generator


def breadthFirstSearch(graphNode,root) :
    q_state = Queue()
    visited = []
    HEAD = PathNode(root,None,None)
    visited.append(HEAD.state)
    adj = state_generator.getActionState(graphNode,root[0],root[1])
    for action in adj:
        q_state.enqueue(PathNode(adj[action],action,HEAD))
    # rep_queue = [x.state for x in q_state.showQ()]
    # print(rep_queue)
    while(not q_state.isEmpty()):
        next_node = q_state.dequeue()
        if next_node.state not in visited :
            visited.append(next_node.state)
            if next_node.state[1] == 0 and next_node.state[3] == 1 :
                return next_node
            adj = state_generator.getActionState(graphNode,next_node.state[0],next_node.state[1])
            for action in adj:
                q_state.enqueue(PathNode(adj[action],action,next_node))
            # rep_queue = [x.state for x in q_state.showQ()]
            # print(rep_queue)
