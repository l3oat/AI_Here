
def getActionState(graphNode,point,cstate):
    allstate = {}
    # check state of block
    if cstate == 0 :
        # next state eat 2 block
        node = graphNode[point]
        # check all adjacent
        if node.left != None:
            left_node = graphNode[node.left]
            # check all caondition here
            if left_node.left != None:
                # create state
                obstruct = 0
                if left_node.obstruct or graphNode[left_node.left].obstruct :
                    obstruct = 1
                goal = 0
                if left_node.goal or graphNode[left_node.left].goal :
                    goal = 1
                allstate['left'] = [node.left,1,obstruct,goal]

        if node.right != None:
            right_node = graphNode[node.right]
            # check all caondition here
            if right_node.right != None:
                # create state
                obstruct = 0
                if right_node.obstruct or graphNode[right_node.right].obstruct :
                    obstruct = 1
                goal = 0
                if right_node.goal or graphNode[right_node.right].goal :
                    goal = 1
                allstate['right'] = [node.right,2,obstruct,goal]
        if node.up != None:
            up_node = graphNode[node.up]
            # check all caondition here
            if up_node.up != None:
                # create state
                obstruct = 0
                if up_node.obstruct or graphNode[up_node.up].obstruct :
                    obstruct = 1
                goal = 0
                if up_node.goal or graphNode[up_node.up].goal :
                    goal = 1
                allstate['up'] = [node.up,3,obstruct,goal]
        if node.down != None:
            down_node = graphNode[node.down]
            # check all caondition here
            if down_node.down != None:
                # create state
                obstruct = 0
                if down_node.obstruct or graphNode[down_node.down].obstruct :
                    obstruct = 1
                goal = 0
                if down_node.goal or graphNode[down_node.down].goal :
                    goal = 1
                allstate['down'] = [node.down,4,obstruct,goal]  

    elif cstate == 1:
        node = graphNode[point]
        if graphNode[node.left].left != None:
            nextleft_node = graphNode[graphNode[node.left].left]
            goal = 0 
            if nextleft_node.goal :
                goal = 1
            # create state
            if not nextleft_node.obstruct :
                allstate['left'] = [nextleft_node.num,0,0,goal]
        if node.right != None:
            goal = 0 
            if graphNode[node.right].goal :
                goal = 1
            if not graphNode[node.right].obstruct :
                allstate['right'] = [node.right,0,0,goal]
        if node.up != None and graphNode[node.left].up != None :
            # create state
            goal = 0
            up_node = graphNode[node.left] 
            if graphNode[node.up].goal or up_node.goal:
                goal = 1
            obstruct = 0
            if graphNode[node.up].obstruct or up_node.obstruct:
                obstruct = 1
            allstate['up'] = [node.up,1,obstruct,goal]
        if node.down != None and graphNode[node.left].down != None :
            # create state
            goal = 0
            down_node = graphNode[node.left] 
            if graphNode[node.down].goal or down_node.goal:
                goal = 1
            obstruct = 0
            if graphNode[node.down].obstruct or down_node.obstruct:
                obstruct = 1
            allstate['down'] = [node.down,1,obstruct,goal]
             
    elif cstate == 2:
        node = graphNode[point]
        if graphNode[node.right].right != None:
            nextright_node = graphNode[graphNode[node.right].right]
            goal = 0 
            if nextright_node.goal :
                goal = 1
            # create state
            if not nextright_node.obstruct :
                allstate['right'] = [nextright_node.num,0,0,goal]
        if node.left != None:
            goal = 0 
            if graphNode[node.left].goal :
                goal = 1
            if not graphNode[node.left].obstruct :
                allstate['left'] = [node.left,0,0,goal]
        if node.up != None and graphNode[node.right].up != None :
            # create state
            goal = 0
            up_node = graphNode[node.right] 
            if graphNode[node.up].goal or up_node.goal:
                goal = 1
            obstruct = 0
            if graphNode[node.up].obstruct or up_node.obstruct:
                obstruct = 1
            allstate['up'] = [node.up,2,obstruct,goal]
        if node.down != None and graphNode[node.right].down != None :
            # create state
            goal = 0
            down_node = graphNode[node.right] 
            if graphNode[node.down].goal or down_node.goal:
                goal = 1
            obstruct = 0
            if graphNode[node.down].obstruct or down_node.obstruct:
                obstruct = 1
            allstate['down'] = [node.down,2,obstruct,goal]
    elif cstate == 3:
        node = graphNode[point]
        if node.left != None and graphNode[node.up].left != None :
            goal = 0
            left_node = graphNode[graphNode[node.up].left]
            if graphNode[node.left].goal or left_node.goal:
                goal = 1
            obstruct = 0
            if graphNode[node.left].obstruct or left_node.obstruct:
                obstruct = 1
            allstate['left'] = [node.left,3,obstruct,goal]
        if node.right != None and graphNode[node.up].right != None :
            goal = 0
            right_node = graphNode[graphNode[node.up].right]
            if graphNode[node.right].goal or right_node.goal:
                goal = 1
            obstruct = 0
            if graphNode[node.right].obstruct or right_node.obstruct:
                obstruct = 1
            allstate['right'] = [node.right,3,obstruct,goal]
        if graphNode[node.up].up != None:
            nextup_node = graphNode[graphNode[node.up].up]
            goal = 0 
            if nextup_node.goal :
                goal = 1
            # create state
            if not nextup_node.obstruct :
                allstate['up'] = [nextup_node.num,0,0,goal]
        if node.down != None:
            goal = 0 
            if graphNode[node.down].goal :
                goal = 1
            if not graphNode[node.down].obstruct :
                allstate['down'] = [node.down,0,0,goal]

    elif cstate == 4:
        node = graphNode[point]
        if node.left != None and graphNode[node.down].left != None :
            goal = 0
            left_node = graphNode[graphNode[node.down].left]
            if graphNode[node.left].goal or left_node.goal:
                goal = 1
            obstruct = 0
            if graphNode[node.left].obstruct or left_node.obstruct:
                obstruct = 1
            allstate['left'] = [node.left,4,obstruct,goal]
        if node.right != None and graphNode[node.down].right != None :
            goal = 0
            right_node = graphNode[graphNode[node.down].right]
            if graphNode[node.right].goal or right_node.goal:
                goal = 1
            obstruct = 0
            if graphNode[node.right].obstruct or right_node.obstruct:
                obstruct = 1
            allstate['right'] = [node.right,4,obstruct,goal]  
        if graphNode[node.down].down != None:
            nextdown_node = graphNode[graphNode[node.down].down]
            goal = 0 
            if nextdown_node.goal :
                goal = 1
            # create state
            if not nextdown_node.obstruct :
                allstate['down'] = [nextdown_node.num,0,0,goal]
        if node.up != None:
            goal = 0 
            if graphNode[node.up].goal :
                goal = 1
            if not graphNode[node.up].obstruct :
                allstate['up'] = [node.up,0,0,goal]
    return allstate


# print(getActionState(graphNode,'7',2))