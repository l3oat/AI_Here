import state_generator

class Queue:
    def __init__(self):
        self.items = []
    def isEmpty(self):
        return self.items == []
    def enqueue(self, item):
        self.items.insert(0,item)
    def dequeue(self):
        return self.items.pop()
    def size(self):
        return len(self.items)
    def showQ(self):
        return self.items

class PathNode:
    def __init__(self,state,a_prev,prev):
        self.state = state
        self.a_prev = a_prev
        self.prev = prev

def getPath(pathnode):
    path = []
    if pathnode==None:
        return None
    while(pathnode.prev!=None):
        path.append(pathnode.a_prev)
        pathnode = pathnode.prev
    path.reverse()
    return path