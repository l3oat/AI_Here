import win32gui
import win32con
import win32api
import win32process
import win32com.client
from time import sleep

VK_CODE = {'left':'{LEFT}',
           'up':'{UP}',
           'right':'{RIGHT}',
           'down':'{DOWN}',
           }

def sendKeySimulate(path):
    # hwndMain = win32gui.FindWindow("Notepad", "Untitled - Notepad")
    # hwndMain = win32gui.FindWindow(None, "NoxPlayer")
    # hwndMain = win32gui.FindWindow(None, "Bloxorz - Block And Hole")
    # hwndChild = win32gui.GetWindow(hwndMain, win32con.GW_CHILD)
    # _, pid = win32process.GetWindowThreadProcessId(hwndMain)
    shell = win32com.client.Dispatch("WScript.Shell")
    shell.AppActivate("Bloxorz - Block And Hole")
    sleep(0.5)
    for i in range(len(path)):
        sleep(0.2)
        print(path[i])
        shell.SendKeys(VK_CODE[path[i]])
        if i == 0:
            sleep(0.5)
        # win32api.PostMessage(hwndChild, win32con.WM_KEYDOWN, VK_CODE[i], 0)
