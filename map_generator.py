from map import Node

# None = '-'
# Normal = '0'
# Start = 'S'
# Obstruct = 'X'
# Goal = 'G'

def mapGen(mapA1):
    graphMap = {}
    startMap = None
    for i in range(len(mapA1)):
        for j in range(len(mapA1[i])):
            if mapA1[i][j] != '-':
                num = i * len(mapA1[i]) + j
                left_num = num-1
                right_num = num+1
                up_num = (i-1) * len(mapA1[i]) + j
                down_num = (i+1) * len(mapA1[i]) + j
                # print(num,left_num,right_num,up_num,down_num)
                node = Node(str(num),None,None,None,None,False,False)
                # check left has tile
                if mapA1[i][j-1] != '-':
                    node.setLeft(str(left_num))
                # check right
                if mapA1[i][j+1] != '-':
                    node.setRight(str(right_num))
                # check up
                if mapA1[i-1][j] != '-':
                    node.setUp(str(up_num))
                # check down
                if mapA1[i+1][j] != '-':
                    node.setDown(str(down_num))
                if mapA1[i][j] == 'X':
                    node.setObstruct(True)
                if mapA1[i][j] == 'G':
                    node.setGoal(True)
                graphMap[str(num)] = node
                if mapA1[i][j] == 'S':
                    startMap = [str(num),0,0,0]   
    return graphMap,startMap