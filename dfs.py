from search_algorithm import PathNode,state_generator


def depthFirstSearch(graphNode,root):
    stack = []
    visited = []
    HEAD = PathNode(root,None,None)
    visited.append(HEAD.state)
    adj = state_generator.getActionState(graphNode,root[0],root[1])
    for action in adj:
        stack.append(PathNode(adj[action],action,HEAD))
    # rep_stack = [x.state for x in stack]
    # print(rep_stack)
    while(stack!=[]):
        next_node = stack.pop()
        if next_node.state not in visited :
            visited.append(next_node.state)
            if next_node.state[1] == 0 and next_node.state[3] == 1 :
                return next_node
            adj = state_generator.getActionState(graphNode,next_node.state[0],next_node.state[1])
            for action in adj:
                stack.append(PathNode(adj[action],action,next_node))  
            # rep_stack = [x.state for x in stack]
            # print(rep_stack)